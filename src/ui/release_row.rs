use gtk::prelude::*;

use crate::ui::utils;

pub struct ReleaseRow {
    pub widget: gtk::ListBoxRow,
    builder: gtk::Builder,
}

impl ReleaseRow {
    pub fn new() -> Self {
        let builder = gtk::Builder::from_resource("/de/haeckerfelix/Souk/gtk/release_row.ui");
        get_widget!(builder, gtk::ListBoxRow, release_list_box);

        Self {
            widget: release_list_box,
            builder,
        }
    }

    pub fn set_release(&self, release: appstream::Release) {
        self.widget.set_visible(true);

        get_widget!(self.builder, gtk::Label, date_label);
        get_widget!(self.builder, gtk::Label, header_label);
        get_widget!(self.builder, gtk::Label, description_label);

        utils::set_date_label(&date_label, release.date);
        header_label.set_text(&format!("New in Version {}", &release.version));
        utils::set_label_markup_translatable_string(&description_label, release.description);
    }

    pub fn reset(&self) {
        get_widget!(self.builder, gtk::Label, date_label);
        get_widget!(self.builder, gtk::Label, header_label);
        get_widget!(self.builder, gtk::Label, description_label);

        date_label.set_text("–");
        header_label.set_text("–");
        description_label.set_text("–");
    }
}
