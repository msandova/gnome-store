mod explore_page;
pub use explore_page::ExplorePage;

mod installed_page;
pub use installed_page::InstalledPage;

mod package_details_page;
pub use package_details_page::PackageDetailsPage;

mod search_page;
pub use search_page::SearchPage;

//TODO: Wouldn't it make sense to add a trait for pages?
