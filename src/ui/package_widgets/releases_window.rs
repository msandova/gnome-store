use glib::WeakRef;
use gtk::prelude::*;

use crate::backend::SoukPackage;
use crate::ui::package_widgets::PackageWidget;
use crate::ui::release_row::ReleaseRow;

#[derive(Clone, Debug)]
pub struct ReleasesWindow {
    pub widget: WeakRef<libhandy::Window>,
    builder: gtk::Builder,
}

impl PackageWidget for ReleasesWindow {
    fn new() -> Self {
        let builder = gtk::Builder::from_resource("/de/haeckerfelix/Souk/gtk/releases_window.ui");
        get_widget!(builder, libhandy::Window, releases_window);

        Self {
            widget: releases_window.downgrade(),
            builder,
        }
    }

    fn set_package(&self, package: &SoukPackage) {
        get_widget!(self.builder, gtk::ListBox, listbox);
        if let Some(appdata) = package.get_appdata() {
            let releases = appdata.releases;
            if releases.is_empty() {
                self.widget.upgrade().unwrap().set_visible(false);
            }
            for release in releases.into_iter().rev() {
                let r = ReleaseRow::new();
                r.set_release(release);
                listbox.prepend(&r.widget);
            }
        }
    }

    fn reset(&self) {
        get_widget!(self.builder, gtk::ListBox, listbox);
        while let Some(widget) = listbox.get_first_child() {
            listbox.remove(&widget);
        }
    }
}

impl ReleasesWindow {
    pub fn show(&self) {
        let app: gtk::Application = gio::Application::get_default().unwrap().downcast().unwrap();
        let window = app.get_active_window().unwrap();
        let releases_window = self.widget.upgrade().unwrap();
        releases_window.set_transient_for(Some(&window));
        releases_window.set_modal(true);
        releases_window.show()
    }
}
