use gtk::prelude::*;

use crate::backend::SoukPackage;
use crate::ui::package_widgets::releases_window::ReleasesWindow;

use crate::ui::package_widgets::PackageWidget;
use crate::ui::release_row::ReleaseRow;

pub struct ReleasesBox {
    pub widget: gtk::Box,
    builder: gtk::Builder,
    release: ReleaseRow,
    releases_window: ReleasesWindow,
}

impl PackageWidget for ReleasesBox {
    fn new() -> Self {
        let builder = gtk::Builder::from_resource("/de/haeckerfelix/Souk/gtk/releases_box.ui");
        get_widget!(builder, gtk::Box, releases_box);
        let release = ReleaseRow::new();
        let releases_window = ReleasesWindow::new();

        let releases_box = Self {
            widget: releases_box,
            builder,
            release,
            releases_window,
        };
        releases_box.setup_signals();
        releases_box
    }

    fn set_package(&self, package: &SoukPackage) {
        let releases = package
            .get_appdata()
            .expect("No appdata available")
            .releases;
        if !releases.is_empty() {
            let release = releases[0].clone();
            self.release.set_release(release);
            get_widget!(self.builder, gtk::ListBox, release_list_box);
            release_list_box.prepend(&self.release.widget);
            self.widget.set_visible(true);
        } else {
            self.widget.set_visible(false);
        }
        self.releases_window.set_package(package);
    }

    fn reset(&self) {
        self.release.reset();
        self.releases_window.reset();
        get_widget!(self.builder, gtk::ListBox, release_list_box);
        if let Some(button) = release_list_box.get_last_child() {
            while let Some(widget) = release_list_box.get_first_child() {
                release_list_box.remove(&widget);
            }
            release_list_box.prepend(&button);
        }
    }
}

impl ReleasesBox {
    fn setup_signals(&self) {
        get_widget!(self.builder, gtk::Button, history_button);
        let releases_window = self.releases_window.clone();
        history_button.connect_clicked(move |_| {
            debug!("Show History Button clicked");
            releases_window.show();
        });
    }
}
